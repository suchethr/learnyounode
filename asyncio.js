var fs = require('fs')

var arr=[]

function readasync(callback) {
fs.readFile(process.argv[2],function doneReading(err, fileContents) {
var str = fileContents.toString()
arr = str.split("\n");
callback()
 })
}

function numberoflines() {
    console.log(arr.length-1)
}

readasync(numberoflines)

/*

     var fs = require('fs')
     var file = process.argv[2]

     fs.readFile(file, function (err, contents) {
       // fs.readFile(file, 'utf8', callback) can also be used
       var lines = contents.toString().split('\n').length - 1
       console.log(lines)
     })

*/
